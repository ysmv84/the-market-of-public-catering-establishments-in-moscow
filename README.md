**Description project:**  
Investors from the "Shut Up and Take My Money" fund are venturing into a new field by opening a food establishment in Moscow. However, they have not yet determined what type of establishment it will be - a cafe, restaurant, pizzeria, pub, or bar - nor have they determined its location, menu, or prices. As an analyst, your task is to conduct market research on Moscow, identify interesting features, and present the results to aid investors in choosing a suitable location in the future.

**Data Description:**  
A dataset of Moscow's food establishments compiled from Yandex Maps and Yandex Business services for the summer of 2022 is available. The information on Yandex Business could have been added by users or obtained from publicly available sources and is solely for reference purposes.

_The file 'moscow_places.csv' includes the following columns:_  

- `name` - the name of the place
- `address` - the address of the place
- `category` - the category of the place, such as "cafe", "pizza place", or "coffee shop"
- `hours` - the days and hours of operation
- `lat` - the latitude of the location of the place
- `lng` - the longitude of the location of the place
- `rating` - the user rating of the place on Yandex Maps (with a maximum rating of 5.0)
- `price` - the price category of the place, such as "average", "below average", "above average", and so on
- `avg_bill` - the average cost of an order in the form of a range, such as "Average bill: 1000-1500 rubles", "Price of a cappuccino: 130-220 rubles", "Price of a beer glass: 400-600 rubles", and so on
- `middle_avg_bill` - the estimated median value for the average bill column (only for values starting with the substring "Average bill")
- `middle_coffee_cup` - the estimated median value for the price of one cappuccino (only for values starting with the substring "Price of a cappuccino")
- `chain` - a binary value (0 or 1) indicating whether the place is part of a chain (for small chains, errors may occur)
- `district` - the administrative district in which the place is located, such as the Central Administrative District
- `seats` - the number of seats available at the place  
Note that the dataset may contain errors for small chains, and some values in the 'avg_bill', 'middle_avg_bill', 'middle_coffee_cup', and 'seats' columns may be missing or inaccurate.

**General Conclusion:**  
In conclusion, our research on the restaurant industry in Moscow, based on data from Yandex Maps and Yandex Business for the summer of 2022, revealed that cafes, restaurants, and coffee shops are in high demand and face tough competition. However, coffee shops have the lowest entry barrier and the most growth potential. A growing number of people prefer to enjoy their coffee outside of their homes, with a significant portion of Russians between the ages of 25 and 45 buying a cup of coffee to take with them to work or school. Based on our data, we recommend that the most promising administrative districts for opening a coffee shop in Moscow are the Central Administrative District and the North Administrative District.
